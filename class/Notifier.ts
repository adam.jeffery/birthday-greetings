import { IFriend } from './DataSource'
import { createTransport, Transporter } from 'nodemailer'
import util from 'util'

/**
 * Factory for creating correct notifier type based on cli command given
 */
export default abstract class NotifierFactory {
    static getNotifier(notifier: string): INotifier {
        switch (notifier) {
            case 'sms': return new SMSNotifier()
            default: return new EmailNotifier()
        }
    }
}
  
/**
 * Interface defintion for any notifier
 */
export interface INotifier {
    send(friend: IFriend, subject: string, greeting: string): Promise<{message: string}>
}
  
/**
 * Class for providing email notification mechanism.
 * 
 * This basic implementation assumes that the sendmail is available on the usr PATH in /usr/sbin/
 */
class EmailNotifier implements INotifier {
    private transport: Transporter

    constructor () {
        this.transport = createTransport({ sendmail: true, path: '/usr/sbin/sendmail' })
    }

    async send (friend: IFriend, subject: string, greeting: string) {
        const html = `<h1>${util.format(greeting, friend.name)}!</h1>`
        await this.transport.sendMail({
            from: 'adamjeffery.dev@gmail.com',
            to: friend.email,
            subject: subject,
            html: html
        })
        const message = `Succesfully sent bithday greeting by email to ${friend.name} at ${friend.email}`
        return { message }
    }
}
  
/**
 * Class for providing SMS notification mechanism
 */
class SMSNotifier implements INotifier {
    async send (_friend: IFriend) {
        // THIS IS WHERE WE'D ACTUALLY HANDLE SENDING AN SMS
        return { message: 'Succesfully sent bithday greeting by SMS...' }
    }
}