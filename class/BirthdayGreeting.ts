import DataSourceFactory, { IDataSource, IFriend } from './DataSource'
import NotifierFactory, { INotifier } from './Notifier'

class BirthdayGreeting {
    private dataSource: IDataSource
    private greeting: string
    private subject: string
    private notifier: INotifier

    constructor (dataSource: string, subject: string, greeting: string, notifier: string, path?: string) {
        if (!dataSource || !subject || !greeting || !notifier) {
            throw new Error('Requires source, subject, greeting and notifier to be injected.')
        }

        this.subject = subject
        this.greeting = greeting
        this.dataSource = DataSourceFactory.getDataSource(dataSource, path)
        this.notifier = NotifierFactory.getNotifier(notifier)
    }

    async run () {
      const friends = await this.dataSource.getFriendList().catch(err => { throw new Error(err) }) as IFriend[]
      const today = new Date()
      const birthdays = friends.filter(friend => { return this.isBirthday(today, new Date(friend.dob)) })

      const results = []
      for(let bday of birthdays) {
        results.push(await this.notifier.send(bday, this.subject, this.greeting))
      }
      return results
    }

    isBirthday (today: Date, dob: Date) {
      const birthdayToday = (today.getDate() === dob.getDate()) && (today.getMonth() === dob.getMonth())
      const birthdayIsLeapYear = (today.getMonth() === 1 && today.getDate() === 28) && (dob.getMonth() === 1 && dob.getDate() === 29)
      return birthdayToday || birthdayIsLeapYear
    }
}

export default BirthdayGreeting