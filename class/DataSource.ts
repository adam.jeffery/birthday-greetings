import { createInterface } from 'readline'
import { createReadStream } from 'fs'

/**
 * Factory for creating correct data source type based on cli command given
 */
export default abstract class DataSourceFactory {
  static getDataSource(dataSource: string, path: string | undefined): IDataSource {
    switch (dataSource) {
        case 'db': return new DBDataSource()
        default: return new FlatFileDataSource(path || '')
    }
  }
}
  
/**
 * Interface defining how a Friend should be represented
 */
export interface IFriend {
  name: string
  dob: string
  email: string
}

/**
 * Interface definition for any data source
 */
export interface IDataSource {
  getFriendList(): Promise<IFriend[]>
}

/**
 * Class to represent database data source
 */
class DBDataSource implements IDataSource {
  async getFriendList () {
      // Just creating a dummy friends list, in the absence of a DB.
      const friends: IFriend[] = [{
        name: 'John Smith',
        dob: '1982/06/04',
        email: 'adamjeffery.dev@gmail.com'
      },
      {
        name: 'Jon Jones',
        dob: '1984/02/28',
        email: 'adamjeffery.dev@gmail.com'
      },
      {
        name: 'Alice Smith',
        dob: '1962/02/29',
        email: 'adamjeffery.dev@gmail.com'
      }]
      
      return friends
  }
}

/**
 * Class to represent local flat file data source
 */
class FlatFileDataSource implements IDataSource {
  private path: string

  constructor (path: string = './flat-file-data') {
    this.path = path
  }

  /**
   * Responsible for generating a friends list from a flat file
   */
  async getFriendList () {
    const friends: IFriend[] = []
    const fileStream = createReadStream(this.path)
    const rl = createInterface({
      input: fileStream,
      crlfDelay: Infinity
    })
    let headers = null
    for await (const line of rl) {
      const parts = line.split(',')
      if (!headers) {
        headers = parts
        continue
      }
      const friend = {
        name: parts[1].trim(),
        dob: parts[2].trim(),
        email: parts[3].trim()
      }
      friends.push(friend)
    }
    return friends
  }
}