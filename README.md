# Submission Details

### Frameworks/Libraries Used

- typescript
- commander
- nodemailer

### Assumptions Made

1. The date provided in the examples were in the format of YYYY/MM/DD

2. Date formats are guaranteed to be consistent

3. Authentication and Authorisation logic etc does not need to be considered for this task

### Installation and Usage

1. Clone this repo down

2. Change directory into cloned project and run npm install and npm run build
  
```
$ cd /path/to/birthday-greetings
$ npm i
$ npm run build
```

3. Usage

This is a CLI tool that can be called by some scheduled task, such as cron or equivalent

You can specify a custom subject, greeting, data source (db|flat-file) and the notification type to use (email|sms)

Email is configured using nodemailer and _will_ send external emails, assuming you have `sendmail` on your PATH. This would ideally be configured with some SMTP server.

SMS has not been configured, but it's class has been defined with some dummy data.

See help command for usage details `node birthday-greetings.js help send`

```
Usage: birthday-greetings send [options]

Send birthday greetings to friends whos birthday it is today!

Options:

-s, --subject [string] subject to use in greeting being sent (default: "Happy birthday!")
-g, --greeting [string] the greeting to use in the notification (default: "Happy birthday, dear %s!")
-d, --data [string] data source of friends list (flat-file|db) (default: "flat-file")
-p, --path [string] if a flat file, a path can be provided (default: "./flat-file-data")
-n, --notifier [string] what notification mechanism to use (email|sms) (default: "email")
-h, --help display help for command
```

Examples

```
$ node birthday-greetings.js send --subject "Happy BDAY mate!" --greeting "Hope you have great day %s, call you soon." --notifier sms
``` 

Could even use it at Christmas etc, not just birthdays...

```
$ node birthday-greetings.js send --subject "Merry Xmas!" --greeting "Merry christmas %s, have a great new year." --notifier sms
```

### Testing

I haven't actually implemented any tests for this, but if I were too I'd use Jest, and unit test each individual function and most likely mock the different notifiers and data sources as a minimum. The `isBirthday` function would also be a key one to thoroughly test edge cases on or may might end up annoying friends :)
