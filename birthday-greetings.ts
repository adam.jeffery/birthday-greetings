import { Command } from 'commander'
import BirthdayGreeting from './class/BirthdayGreeting'

const program = new Command()

program
  .name('birthday-greetings')
  .description('CLI to send birthday notifications to friends...')
  .version('1.0.0')

program.command('send')
  .description('Send birthday greetings to friends who\s birthday it is today!')
  .option('-s, --subject [string]', 'subject to use in greeting being sent', 'Happy birthday!')
  .option('-g, --greeting [string]', 'the greeting to use in the notification', 'Happy birthday, dear %s!')
  .option('-d, --data [string]', 'data source of friends list (flat-file|db)', 'flat-file')
  .option('-p, --path [string]', 'if a flat file, a path can be provided', './flat-file-data')
  .option('-n, --notifier [string]', 'what notification mechanism to use (email|sms)', 'email')
  .action(async (options) => {
    const process = new BirthdayGreeting(options.data, options.subject, options.greeting, options.notifier, options.path)
    const result = await process.run()
    
    console.log(result)
  })

program.parse()